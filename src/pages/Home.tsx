// External libraries
import React from "react";
import { Link as RouterLink } from "react-router-dom";
import { Heading, VStack, Flex } from "@chakra-ui/react";

// Components
import { Wrapper } from "../components/Wrapper";
import { Header } from "../components/Header";

export const Home: React.VFC = () => {
  return (
    <>
      <VStack p={5}>
        <Flex w="100%" mb={10}>
          <Heading
            size="lg"
            fontWeight="semibold"
            fontFamily="comfortaa"
            color="cyan.800"
            as={RouterLink}
            to="/"
          >
            S&E Store
          </Heading>
          {/* TODO: Separate into pages; Implement login */}
          {/* <Spacer />
          <Button as={RouterLink} to="/cart">
            <BiCart />
          </Button> */}
        </Flex>
        <Header />
      </VStack>
      <Wrapper />
    </>
  );
};
