import { ItemContent } from "./ItemContent";

export interface CartItem {
  item: ItemContent;
  quantity: number;
}
