export interface ItemContent {
  id: number;
  name: string;
  price: number;
}
