import axios from "axios";

export const getResponse = async () => {
  const endpoint = "http://34.130.252.55:8080/query";
  // const endpoint = "http://localhost:8000/query";
  const query = `
		{
			items {
				id
				name
				price
			}
		}
	`;
  const headers = {
    "Content-Type": "application/json",
    Accept: "application/json",
    "Access-Control-Allow-Origin": "*",
  };
  try {
    const response = await axios.post<any>(
      endpoint,
      { query: query },
      { headers }
    );
    return response.data;
  } catch (err) {
    console.log(err);
  }
};
