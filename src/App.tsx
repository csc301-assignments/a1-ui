import React from "react";
import {
  BrowserRouter as Router,
  // Uncomment when create multiple pages
  // Switch,
  // Route,
  // Link as RouterLink,
} from "react-router-dom";

// local imports
import { Home } from "./pages/Home";

export const App: React.VFC = () => {
  return (
    <Router>
      {/* <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/cart">
          <Cart />
        </Route>
      </Switch> */}
      <Home />
    </Router>
  );
};
