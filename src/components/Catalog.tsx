// External libraries
import React from "react";
import { Stack, useMediaQuery } from "@chakra-ui/react";

// Components
import { Item } from "./Item";

// Interfaces
import { ItemContent } from "../interfaces/ItemContent";
import { CartItem } from "../interfaces/CartItem";

interface Props {
  items: ItemContent[];
  state: {
    cart: CartItem[];
    setCart: React.Dispatch<React.SetStateAction<CartItem[]>>;
  };
}

export const Catalog: React.VFC<Props> = ({ items, state }: Props) => {
  const [isNotPhone] = useMediaQuery("(min-width: 800px)");
  return (
    <Stack
      w="100%"
      maxW={{ base: "100vh", md: "130vh", lg: "140vh", xl: "150vh" }}
      direction={isNotPhone ? "row" : "column"}
      alignItems="center"
      justify="center"
      gridGap={5}
    >
      {items.map((item) => {
        return <Item key={item.id} item={item} state={state} />;
      })}
    </Stack>
  );
};
