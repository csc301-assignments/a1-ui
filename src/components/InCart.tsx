// External libraries
import React from "react";
import {
  VStack,
  Flex,
  Box,
  useMediaQuery,
  Text,
  Button,
  HStack,
  Image,
} from "@chakra-ui/react";

// Interfaces
import { CartItem } from "../interfaces/CartItem";

interface Props {
  key: string;
  item: CartItem;
  state: {
    cart: CartItem[];
    setCart: React.Dispatch<React.SetStateAction<CartItem[]>>;
  };
}

const changeQuantity = (
  carts: CartItem[],
  item: CartItem,
  amount: number
): CartItem[] => {
  let clone = [...carts];
  let index;
  if ((index = clone.indexOf(item)) === -1) {
    return clone;
  } else if (item.quantity + amount <= 0) {
    clone.splice(index, 1);
  } else {
    clone[index].quantity += amount;
  }
  console.log("cart: ", clone);
  return clone;
};

export const InCart: React.VFC<Props> = ({ item, state }: Props) => {
  const [isNotPhone] = useMediaQuery("(min-width: 800px)");
  const content = item;

  return (
    <Flex
      rounded="xl"
      w={isNotPhone ? "70vh" : "40vh"}
      h="15vh"
      bgColor="blue.300"
      alignItems="center"
      direction="row"
      p={5}
    >
      <Box
        rounded="lg"
        w="12vh"
        h="12vh"
        mr="2vh"
        // bgColor="orange.100"
        justifySelf="left"
      >
        <Image
          boxSize="12vh"
          borderRadius="full"
          src="https://cdn.discordapp.com/attachments/551438576278175776/893392925172563968/Soup.jpg"
        ></Image>
      </Box>
      <VStack>
        <Text w={isNotPhone ? "50vh" : "20vh"} textAlign="left">
          {content.item.name}
        </Text>
        <HStack w={isNotPhone ? "50vh" : "20vh"}>
          <Button
            colorScheme="blue"
            size="sm"
            onClick={() => state.setCart(changeQuantity(state.cart, item, -1))}
          >
            -
          </Button>
          <Text textAlign="left">{content.quantity} pieces</Text>
          <Button
            colorScheme="blue"
            size="sm"
            onClick={() => state.setCart(changeQuantity(state.cart, item, +1))}
          >
            +
          </Button>
        </HStack>
        <Text w={isNotPhone ? "50vh" : "20vh"} textAlign="right">
          ${(content.item.price * content.quantity).toFixed(2)} CAD
        </Text>
      </VStack>
    </Flex>
  );
};
