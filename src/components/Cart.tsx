// External libraries
import React from "react";
import {
  Stack,
  Heading,
  VStack,
  Flex,
  useMediaQuery,
  Text,
  HStack,
  Box,
} from "@chakra-ui/react";

// Components
import { InCart } from "./InCart";

// Interfaces
import { CartItem } from "../interfaces/CartItem";

interface Props {
  state: {
    cart: CartItem[];
    setCart: React.Dispatch<React.SetStateAction<CartItem[]>>;
  };
}

export const Cart: React.VFC<Props> = ({ state }: Props) => {
  const [isNotPhone] = useMediaQuery("(min-width: 800px)");

  const getSubTotal = (inCarts: CartItem[]): number => {
    let sum = 0;
    inCarts.forEach((inCart) => {
      sum += inCart.item.price * inCart.quantity;
    });
    return sum;
  };

  return (
    <VStack p={5} bgColor="cyan.300">
      <Flex w="100%" mb={10}>
        <Heading
          size="lg"
          fontWeight="semibold"
          fontFamily="comfortaa"
          color="cyan.800"
        >
          Shopping Cart
        </Heading>
      </Flex>
      <Stack
        w="100%"
        maxW={{ base: "130vh", md: "130vh", lg: "130vh", xl: "130vh" }}
        direction="column"
        alignItems="center"
        justify="center"
        gridGap={5}
      >
        {state.cart.map((inCart) => {
          console.log(
            "check: ",
            inCart.item.id.toString() + inCart.quantity.toString()
          );
          return (
            <InCart
              key={
                "id" +
                inCart.item.id.toString() +
                "quantity" +
                inCart.quantity.toString()
              }
              item={inCart}
              state={state}
            />
          );
        })}
        {/* TODO: Modularize this */}
        <Box fontWeight="semibold">
          <HStack>
            <Text w={isNotPhone ? "55vh" : "25vh"} textAlign="right">
              Sub-total:
            </Text>
            <Text w="10vh" textAlign="right">
              ${getSubTotal(state.cart).toFixed(2)}
            </Text>
            <Text>CAD</Text>
          </HStack>
          <HStack>
            <Text w={isNotPhone ? "55vh" : "25vh"} textAlign="right">
              Tax:
            </Text>
            <Text w="10vh" textAlign="right">
              ${(getSubTotal(state.cart) * 0.13).toFixed(2)}
            </Text>
            <Text>CAD</Text>
          </HStack>
          <HStack>
            <Text w={isNotPhone ? "55vh" : "25vh"} textAlign="right">
              Total:
            </Text>
            <Text w="10vh" textAlign="right">
              ${(getSubTotal(state.cart) * 1.13).toFixed(2)}
            </Text>
            <Text>CAD</Text>
          </HStack>
        </Box>
      </Stack>
    </VStack>
  );
};
