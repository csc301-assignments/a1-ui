// External libraries
import React from "react";
import { Image, Stack, Text, useMediaQuery } from "@chakra-ui/react";

export const Header: React.VFC = () => {
  const [isNotPhone] = useMediaQuery("(min-width: 600px)");
  return (
    <Stack w="100%" direction="column" alignItems="center" justify="center">
      <Image
        borderRadius="full"
        boxSize={isNotPhone ? "400px" : "300px"}
        src="https://cdn.discordapp.com/attachments/551438576278175776/893392925172563968/Soup.jpg"
      ></Image>
      <Text
        fontFamily="comfortaa"
        fontWeight="semibold"
        fontSize="6xl"
        textAlign="center"
      >
        Welcome to S&E Store!
      </Text>
    </Stack>
  );
};
