// External libraries
import React from "react";
import { VStack, Text, Button, Image, Center } from "@chakra-ui/react";

// Interfaces
import { ItemContent } from "../interfaces/ItemContent";
import { CartItem } from "../interfaces/CartItem";

interface Props {
  key: number;
  item: ItemContent;
  state: {
    cart: CartItem[];
    setCart: React.Dispatch<React.SetStateAction<CartItem[]>>;
  };
}

export const Item: React.VFC<Props> = ({ item, state }: Props) => {
  const handleAddToCart = () => {
    if (!state.cart.some((inCart) => inCart.item.id === item.id)) {
      // Item has never been aded
      state.setCart([...state.cart, { item: item, quantity: 1 }]);
    }
    // TODO: Add an alertbox saying the item has already been added
  };

  return (
    <VStack rounded="xl" w="25vh" h="30vh" bgColor="blue.300" justify="center">
      <Center rounded="xl" h="10vh" w="80%">
        <Image
          boxSize="10vh"
          borderRadius="full"
          src="https://cdn.discordapp.com/attachments/551438576278175776/893392925172563968/Soup.jpg"
        ></Image>
      </Center>
      <Text>{item.name}</Text>
      <Text>{`$${item.price.toFixed(2)} CAD`}</Text>
      <Button
        size="sm"
        colorScheme="blue"
        variant="solid"
        onClick={handleAddToCart}
      >
        Add to cart
      </Button>
    </VStack>
  );
};
