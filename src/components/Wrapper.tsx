// External libraries
import React, { useState, useEffect } from "react";
import { VStack } from "@chakra-ui/react";

// Components
import { Catalog } from "./Catalog";
import { Cart } from "./Cart";

// Interfaces
import { ItemContent } from "../interfaces/ItemContent";
import { CartItem } from "../interfaces/CartItem";

// Others
import { getResponse } from "../functions/getResponse";

export const Wrapper: React.VFC = () => {
  const [items, setItems] = useState<ItemContent[]>([]);
  const [cart, setCart] = useState<CartItem[]>([]);

  // Loading all available items
  useEffect(() => {
    getResponse().then((data: any) => {
      setItems(data.data.items);
    });
  }, []);

  useEffect(() => {
    setCart(cart);
  }, [cart]);

  return (
    <>
      <VStack w="100%" p={5}>
        <Catalog items={items} state={{ cart, setCart }} />
      </VStack>
      <Cart state={{ cart, setCart }} />
    </>
  );
};
